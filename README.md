# Jutge ITB

Jutge ITB es un programa que permet als usuaris aprendre a programar a partir de diferents problemes.

Si ets professor has de fer servir la contrassenya: Jutge2
Ser professor permet crear, eliminar y veure com va l'alumne en el programa.

## Instal·lació

Link de descarrega: https://gitlab.com/raul.argemi.7e6/jutge/-/tree/Ampliacio2?ref_type=heads

## Ús
Primer de tot com s'ha dit abans hi haura un pregunta per veure si ets alumne o professor:
                                                              

# SI ETS ALUMNE:
1. Seguir amb l’itinerari d’aprenentatge
2. Llista problemes
3. Consultar històric de problemes resolts
4. Ajuda
5. Sortir
--------------------------------------------------------

- Seguir amb l’itinerari d’aprenentatge:

Hauras de fer problema y poder completarlos per poder pasar al seguent.

- Llista problemes:

L’opció de llista de problemes mostra una llista amb tots els problemes i en podras seleccionar el que vulguis.

- Consultar històric de problemes resolts

L’històric de problemes resolts mostra els intents realitzats a cada problema i si aquest ha estat superat o no.

- Ajuda
Text d'ajuda.

- Sortir
Sortir de l'aplicació.


# SI ETS PROFESSOR:

Contrassenya: Jutge2

1. Afegir problemes
2. Informacio alumne
3. Mostrar problemes
4. Borrar Problemes
0. Sortir


- Afegir problemes

Hauras de completar els camps que es demanaran i d'aquesta forma es creara un problema amb els valors introduits.

-Informacio alumne

Veuras com va l'alumne en el seu proces d'aprenentatje.

-Mostrar problemes

Veuras els problemas de forma grafica, es a dir vermell si no esta superat, groc si l'ha superat amb molts intents y verd si l'ha superat bé.

-Borrar Problemes

Podras borrar problemes a partir de l'id del problema.

-Sortir

Surts del menú.


# Problemes

He tingut problemes sobretot amb les conexions y amb les taules degut a que hi habia coses que les tenia posades en arrays y he hagut de cambiar parts del codi per poder amoldar-ho tot per poder crear la base de dades.
